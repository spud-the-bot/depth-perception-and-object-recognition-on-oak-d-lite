#!/usr/bin/python3.8 -u

# ----------------------------------------------------------------------------------------------------------------------
#
# controllerHandler.py
# Author: Mike Schoonover
# Date: 07/04/21
#
# Purpose:
#
# Handles interface with a Controller via Ethernet link. Accepts an incoming connection request and then monitors
# the connection for packets from the Controller.
#
# Packets are verified and then made available to client code.
#
# ----------------------------------------------------------------------------------------------------------------------

import sys
import traceback
from typing import List, Tuple, Final

from spudLink.ethernetLink import EthernetLink
from spudLink.packetTool import PacketTool
from spudLink.packetTypeEnum import PacketTypeEnum
from spudLink.spudLinkExceptions import SocketBroken


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
# class ControllerHandler
#
# This class handles interfacing with the Controller device.
#


class ControllerHandler:
    # final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::__init__
    #

    """"
        ControllerHandler initializer.

        :param pThisDeviceIdentifier: a numeric code to identify this device on the network
        :type pThisDeviceIdentifier: int
        :param pRemoteDescriptiveName: a human friendly name for the connected remote device
        :type pRemoteDescriptiveName: str
    """

    def __init__(self, pThisDeviceIdentifier: int, pRemoteDeviceIdentifier: int, pRemoteDescriptiveName: str):

        self.thisDeviceIdentifier: int = pThisDeviceIdentifier

        self.remoteDeviceIdentifier: int = pRemoteDeviceIdentifier

        self.packetTool = PacketTool(self.thisDeviceIdentifier)

        self.port: Final[int] = 4242

        self.ethernetLink = EthernetLink(pRemoteDescriptiveName, self.port)

        self.pktRcvCount = 0

        self.waitingForACKPkt: bool = False

        self.handDataSendTimerEnd: float = 0

        self.HAND_DATA_SEND_TIMER_PERIOD: Final[float] = 0.3

        self.MAX_SHORT_INT: Final[int] = 32767

        self.UNKNOWN_DIGIT_POSITION: Final[int] = self.MAX_SHORT_INT
        self.DIGIT_RETRACTED: Final[int] = self.MAX_SHORT_INT - 1
        self.DIGIT_EXTENDED_UP: Final[int] = 0
        self.DIGIT_EXTENDED_SIDE: Final[int] = 90

        # hand directions upwards/sideways

        self.UPWARDS: Final[int] = 0
        self.SIDEWAYS: Final[int] = 1

        # left right handedness

        self.LEFT_HAND: Final[float] = 0.0
        self.RIGHT_HAND: Final[float] = 1.0

        self.LEFT_HAND_INT: Final[int] = 0
        self.RIGHT_HAND_INT: Final[int] = 1

        # index positions of the x,y coordinates

        self.LM_X_COORD: Final[int] = 0
        self.LM_Y_COORD: Final[int] = 1

        # index positions of the landmarks of the digits
        #
        # Example usage to retrieve x,y of tip of thumb:
        #
        # thumbX = hand.landmarks[self.LM_THUMB_TIP][LM_X_COORD]
        # thumbY = hand.landmarks[self.LM_THUMB_TIP][LM_Y_COORD]
        #

        self.LM_PALM_BASE: Final[int] = 0

        self.LM_THUMB_BASE: Final[int] = 1
        self.LM_THUMB_BASE_ABUT: Final[int] = 2
        self.LM_THUMB_TIP_ABUT: Final[int] = 3
        self.LM_THUMB_TIP: Final[int] = 4

        self.LM_INDEX_BASE: Final[int] = 5
        self.LM_INDEX_BASE_ABUT: Final[int] = 6
        self.LM_INDEX_TIP_ABUT: Final[int] = 7
        self.LM_INDEX_TIP: Final[int] = 8

        self.LM_MIDDLE_BASE: Final[int] = 9
        self.LM_MIDDLE_BASE_ABUT: Final[int] = 10
        self.LM_MIDDLE_TIP_ABUT: Final[int] = 11
        self.LM_MIDDLE_TIP: Final[int] = 12

        self.LM_RING_BASE: Final[int] = 13
        self.LM_RING_BASE_ABUT: Final[int] = 14
        self.LM_RING_TIP_ABUT: Final[int] = 15
        self.LM_RING_TIP: Final[int] = 16

        self.LM_LITTLE_BASE: Final[int] = 17
        self.LM_LITTLE_BASE_ABUT: Final[int] = 18
        self.LM_LITTLE_TIP_ABUT: Final[int] = 19
        self.LM_LITTLE_TIP: Final[int] = 20

    # end of ControllerHandler::__init__
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::getConnected
    #

    def getConnected(self) -> bool:

        """
           Returns the 'connected' flag from EthernetLink which is true if connected to remote and false otherwise.
           :return: 'connected' flag from EthernetLink object
           :rtype: int
        """

        return self.ethernetLink.getConnected()

    # end of ControllerHandler::getConnected
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::setWaitingForACKPkt
    #

    def setWaitingForACKPkt(self, pState: bool):

        self.waitingForACKPkt = pState

    # end of ControllerHandler::setWaitingForACKPkt
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::doRunTimeTasks
    #

    def doRunTimeTasks(self) -> int:

        """

            Handles communications and actions with the remote device. This function should be called often during
            runtime to allow for continuous processing.

            If the remote device is not currently connected, then this function will check for connection requests
            and accept the first one to arrive. Afterwards, this function will monitor the incoming data stream for
            packets and process them.

            Currently, only one remote device at a time is allowed to be connected.

            :return: 0 if no operation performed, 1 if an operation performed, -1 on error
            :rtype: int

        """

        try:

            if not self.ethernetLink.getConnected():
                status: int = self.ethernetLink.connectToRemoteIfRequestPending()
                if status == 1:
                    self.packetTool.setStreams(self.ethernetLink.getInputStream(), self.ethernetLink.getOutputStream())
                    return 1
                else:
                    return 0
            else:
                return self.handleCommunications()

        except ConnectionResetError:

            self.logExceptionInformation("Connection Reset Error - Host program probably terminated improperly.")
            self.disconnect()
            return 0

        except ConnectionAbortedError:

            self.logExceptionInformation("Connection Aborted Error - Host program probably terminated improperly.")
            self.disconnect()
            return 0

        except SocketBroken:
            self.logExceptionInformation("Host Socket Broken - Host probably closed connection.")
            self.disconnect()
            return 0

    # end of ControllerHandler::doRunTimeTasks
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::handleCommunications
    #

    def handleCommunications(self) -> int:

        """
            Handles communications with the remote device. This function should be called often during runtime to allow
             for continuous processing.

            This function will monitor the incoming data stream for packets and process them as they are received.

            :return: 0 on no packet handled, 1 on packet handled, -1 on error
                        note that broken or disconnected sockets do NOT return an error as they are handled as a
                        normal part of the process
            :rtype: int

            :raises: SocketBroken:  if socket is broken - probably due to Host closing connection

        """

        self.ethernetLink.doRunTimeTasks()

        packetReady: bool = self.packetTool.checkForPacketReady()

        if packetReady:
            return self.handlePacket()

        self.pktRcvCount += 1

        return 0

    # end of ControllerHandler::handleCommunications
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::handlePacket
    #

    def handlePacket(self) -> int:

        """
            Handles a packet received from the remote device.

            :return: 0 on no packet handled, 1 on packet handled, -1 on error
            :rtype: int
        """

        pktType: PacketTypeEnum = self.packetTool.getPktType()

        if pktType is PacketTypeEnum.GET_DEVICE_INFO:

            return self.handleGetDeviceInfoPacket()

        elif pktType == PacketTypeEnum.LOG_MESSAGE:

            # todo mks
            print("Packet received of type: " + pktType.name + "\n")
            return 1

        else:

            return 0

    # end of ControllerHandler::handlePacket
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::handleGetDeviceInfoPacket
    #

    def handleGetDeviceInfoPacket(self) -> int:

        """
            Handles a GET_DEVICE_INFO packet by transmitting a greeting via a LOG_MESSAGE packet back to the remote
            device.

            :return: 0 on no packet handled, 1 on packet handled, -1 on error
            :rtype: int
        """

        print("Packet received of type: GET_DEVICE_INFO")

        self.packetTool.sendString(self.remoteDeviceIdentifier,
                                   PacketTypeEnum.LOG_MESSAGE, "Hello from the Pose Recognizer Server Daemon!")

        return 1

    # end of ControllerHandler::handleGetDeviceInfoPacket
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::sendSignedShortIntsFromListOfTuples
    #

    def sendSignedShortIntsFromListOfTuples(self, pPacketType: PacketTypeEnum,
                                            pData: List[Tuple[int, ...]]) -> bool:

        """
            Sends a series of signed short ints (16 bits sent as 2 bytes each) to the remote device, prepending a
            valid header and appending the appropriate checksum.

            The short ints will be read from a List of Tuples. The List can contain unlimited Tuples and each Tuple
            can contain unlimited ints. Only the two least significant bytes of each Python int will be sent.

            The values are sent Big Endian.

            If the byte series along with the header and checksum will not fit into the output buffer, the series
            will be truncated as required

            :param pPacketType: the packet type code
            :type pPacketType:  PacketTypeEnum
            :param pData:       the List of Tuples containing ints of which the two least significant bytes of each
                                are to be sent
            :type pData:        List[Tuple[int]]
            :return:            true if no error, false on error
            :rtype:             bool

            :raises: SocketBroken:  if the socket is closed or becomes inoperable

         """

        return self.packetTool.sendSignedShortIntsFromListOfTuples(self.remoteDeviceIdentifier, pPacketType, pData)

    # end of ControllerHandler::sendSignedShortIntsFromListOfTuples
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::disconnect
    #

    def disconnect(self) -> int:

        """
            Disconnects from the Controller device and resets the PacketTool which discards any partially read
            packets.

            :return: 0 if successful, -1 on error
            :rtype: int
        """

        self.packetTool.reset()

        return self.ethernetLink.disconnect()

    # end of ControllerHandler::disconnect
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # ControllerHandler::logExceptionInformation
    #

    @staticmethod
    def logExceptionInformation(pMessage: str):

        """
            Displays a message, the current Exception name and info, and the traceback info.

            A row of asterisks is printed before and after the info to provide separation.

            :param pMessage: the message to be displayed
            :type pMessage: str
        """

        print("***************************************************************************************")

        print(pMessage)

        print("")

        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)

        print("***************************************************************************************")

    # end of ControllerHandler::logExceptionInformation
    # --------------------------------------------------------------------------------------------------

# end of class ControllerHandler
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
