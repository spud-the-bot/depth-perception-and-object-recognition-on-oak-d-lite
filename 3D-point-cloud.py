"""
This example will calculate depth from disparity, and compare it to the depth calculated on the OAK camera.
"""

import depthai as dai
import cv2
import numpy as np
from skimage.metrics import structural_similarity as ssim

pipeline = dai.Pipeline()

# Define sources and outputs
stereo = pipeline.createStereoDepth()
stereo.setLeftRightCheck(False)
stereo.setExtendedDisparity(False)
stereo.setSubpixel(True)

# Properties
monoLeft = pipeline.createMonoCamera()
monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoLeft.out.link(stereo.left)

monoRight = pipeline.createMonoCamera()
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
monoRight.out.link(stereo.right)

#xDisp = pipeline.createXLinkOut()
#xDisp.setStreamName("disparity")
#stereo.disparity.link(xDisp.input)

xDepth = pipeline.createXLinkOut()
xDepth.setStreamName("depth")
stereo.depth.link(xDepth.input)

# Connect to device and start pipeline

with dai.Device(pipeline) as device:

    #qDisp = device.getOutputQueue(name="disparity", maxSize=1, blocking=True)
    qDepth = device.getOutputQueue(name="depth", maxSize=1, blocking=True)

    calib = device.readCalibration()
    baseline = calib.getBaselineDistance(useSpecTranslation=True) * 10  # mm
    intrinsics = calib.getCameraIntrinsics(dai.CameraBoardSocket.RIGHT, monoRight.getResolutionSize())
    focalLength = intrinsics[0][0]
    disp_levels = stereo.initialConfig.getMaxDisparity() / 95
    dispScaleFactor = baseline * focalLength * disp_levels

    while True:

        # WARNING:
        # it is REQUIRED to retrieve one dispFrame for each depthFrame or the pipeline will lock up and freeze

        #dispFrame = np.array(qDisp.get().getFrame())
        depthFrame = qDepth.get().getCvFrame();

        # scale back to integer disparities, opencv has 4 subpixel bits
        # disparity_scaled = (self.disparity / 16.).astype(np.uint8)

#        depthColorMap = cv2.applyColorMap(
#            (disparity_scaled * (256. / self.max_disparity)).astype(np.uint8),
#            cv2.COLORMAP_HOT)

        maxDisparity = 96

        # scale back to integer disparities, opencv has 4 subpixel bits
        depthFrameScaled = (depthFrame / 16.).astype(np.uint8)

        depthColorMap = cv2.applyColorMap(
            (depthFrameScaled * 2.7).astype(np.uint8),
            cv2.COLORMAP_VIRIDIS)

        # cv2.COLORMAP_VIRIDIS


        cv2.imshow("Depth Map", depthFrame)
        cv2.imshow("Depth Color Map", depthColorMap)

        if cv2.waitKey(1) == ord('q'):
            break
